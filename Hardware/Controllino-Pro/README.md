# Controllino Pro

Newest hotspot, it is a design iteration of v1. The most of modifications were made do the aluminium case. It is identical to v1 from software point of view.

# Internal parts

For internal parts details [take a look at v1](/Hardware/Controllino-v1).

# External parts

Case

- aluminium case
- glued
- ventilation slots
- SMA female connector
  - ![SMA female connector](/Hardware/Controllino-v1/.img/SMA_female.png "SMA female")
- Gigabit ethernet
- WiFi
- 2 USB A ports
- dimensions 140x68x38 mm

Power connector

- 5.1V/3A Power Supply with USB C connector

Antenna

- 2.2dBi LoRa 868MHz Antenna, SMA Male connector

# Images

![closed case](/Hardware/Controllino-Pro/.img/controllino-pro_1.jpg "Closed case ")
![open case 1](/Hardware/Controllino-Pro/.img/controllino_pro_2.jpg "Open case 1")
![open case 2](/Hardware/Controllino-Pro/.img/controllino_pro_3.jpg "Open case 2")   
