# Check error logs of the miner

## Option 1: using Dashboard

- connect to your miner locally or using a vpn connection
- access the Dashboard using your browser
- click on |Actions| at the bottom
- click on |Miner Error LOG| and wait for it to load
- click on |Miner Console LOG| and wait for it to load

## Option 2: using SSH

- connect to your miner using SSH
- enter the command cat `/var/log/miner/console.log | more`
- enter the command `cat /var/log/miner/error.log | more`
- This will scroll the full log in pages
- press `down` or `space` key for next page
