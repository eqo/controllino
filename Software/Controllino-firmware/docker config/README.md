# checking docker.config file
The docker.config file is crucial to a working miner. This is how you check and adjust it's content if needed.

## Windows

- use [PuTTY](https://www.putty.org/) or the built in SSH client to connect to your miner
- for PuTTY enter the IP address of your miner and port 22
- click on |Open|
- accept the security risk warning
- enter your SSH username and password at the prompt

## Linux

- on terminal and connect to your miner using the IP and comand `ssh pi@IP`
- enter `yes` if you get a warning message
- enter your password when prompted

## Mac

- launch terminal and connct to your miner using the IP and comand `ssh pi@IP`
- enter `yes` if you get a warning message
- enter your password when prompted

## check file contents

- check the contents of **docker.config** file using the command `cat /home/pi/docker_config/docker.config`
- compare the file contents with this file [docker.config](/Software/Controllino-firmware/docker config/docker.config)
- if there are differences adjust the content as stated below
- ✔ if it is the same you have the correct version of docker.config
- type `logout` to finish your session

## adjust file contents

- open the **docker.config** file in the editor using the command `sudo nano /home/pi/docker_config/docker.config`
- if asked for a password enter your admin password
- use the arrow keys to navigate to the part that differs and adjust it
- hit CTRL+O to save your changes
- hit CTRL+X to quit nano
- ✔ you have now the correct version of docker.config
- type `logout` to finish your session


