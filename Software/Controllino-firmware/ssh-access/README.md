# Enabling SSH Access

> ⚠ It is always a good idea to create a backup of the SD card first.

For a visual guide you can watch the [video by @Uros](https://www.youtube.com/watch?v=msOSEU4U0MU&t=510s).

## Option 1: physical access to SD card

Credits to [@DimitriSar](https://github.com/DimitrisSar/Controllino#backup)

- place a blank file named `ssh` onto the boot partition of your SD Card
- Mount the ext4 partition and create a folder `.ssh` in directory `/home/pi/`
- Create a text file `authorized_keys` in directory `/home/pi/.ssh/` and paste your public ssh key inside
- put the SD card back into your hotspot
- connect using any SSH client with user **pi** and **your key**
- ✔ You now have SSH access to your hotspot

## Option 2: using controllino dashboard 1.4.3 

- log into dashboard and click action |SSH ACCESS|
- read the message in the popup
- enter an email address you have access to
- SSH access is activated an a 6 digit pin sent to your inbox
- enter the pin to see the SSH password
- **ATTENTION**: don't click anywhere until you have noted down the password! It is only showed once.
- connect using any SSH client with user **admin** and **your password**
- ✔ You now have SSH access to your hotspot


#### Knonw problems

- some email providers don't receive the email containing the pin (e.g GMX)
  - try an email address from a different provider
