# Flashing an SD card image 
How to flash an image to your miner.<br>
> ⚠ Note the content of the SD card will be overwritten. You might want to try this on a spare SD card first.

For a visual guide you can watch the [video by @Uros.](https://www.youtube.com/watch?v=msOSEU4U0MU&t=320s)

## Windows

- Download and install [Win32 Disk Imager](https://sourceforge.net/projects/win32diskimager/)
- chose the image file
- chose the device (drive letter) you want to write to
- click on |Write|
- ✔ you now have restored the image to your SD card
- put the SD card back into the miner and boot it to check if it works correctly

## Linux

- find the device name of your sd card using command `sudo fdisk -l` or looking into a graphical disk utility. The disc model should be 'Card Reader'.
- in our example the device name is **/dev/sde** and we will read the image from the directory **/home/username/**
- remember to change the device name and directory name in the command accordingly
- use the dd command to flash the image: `sudo dd bs=4M if=/home/username/MyImage.img of=/dev/sde status=progress`
- ✔ you now have restored the image to your SD card
- put the SD card back into the miner and boot it to check if it works correctly

## Mac

If you are looking for a graphical tool you can use [ApplePi Baker](https://www.tweaking4all.com/software/macosx-software/applepi-baker-v2/), which is straight forward to use.

For the terminal approach follow:

- launch a terminal window and find the device name of your sd card using command `sudo diskutil list`
- in our example the device name is **/dev/sde** and we will read the image from the directory **/home/username/**
- remember to change the device name and directory name in the command accordingly
- use the dd command to flash the image: `sudo dd bs=4M if=/home/username/MyImage.img of=/dev/sde status=progress`
- ✔ you now have restored the image to your SD card
- put the SD card back into the miner and boot it to check if it works correctly
