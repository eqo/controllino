# Create a backup of your SD card
How to create a backup of your current miner software

For a visual guide you can watch the [video by @Uros](https://www.youtube.com/watch?v=msOSEU4U0MU&t=405s).

## Windows

Credits to [@DimitrisSar](https://github.com/DimitrisSar/Controllino#backup)

- Download and install [Win32 Disk Imager](https://sourceforge.net/projects/win32diskimager/)
- chose a folder to store your backup and click |Read|
- ✔ you now have a backup of your miner

## Linux

- find the device name of your sd card using command `sudo fdisk -l` or looking into a graphical disk utility. The disc model should be 'Card Reader'.
- in our example the device name is **/dev/sde** and we will store it to the directory **/home/username/**
- remember to change the device name and output directory in the command accordingly
- use the dd command to create the image: `sudo dd bs=4M if=/dev/sde of=/home/username/MyImage.img status=progress`
- ✔ you now have a backup of your miner

## Mac

If you are looking for a graphical tool you can use [ApplePi Baker](https://www.tweaking4all.com/software/macosx-software/applepi-baker-v2/), which is straight forward to use.

For the terminal approach follow:

- launch a terminal window and find the device name of your sd card using command `sudo diskutil list`
- in our example the device name is **/dev/sde** and we will store it to the directory **/home/username/**
- remember to change the device name and output directory in the command accordingly
- use the dd command to create the image: `sudo dd bs=4M if=/dev/sde of=/home/username/MyImage.img status=progress`
- ✔ you now have a backup of your miner
