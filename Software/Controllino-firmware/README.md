# Controllino firmware
Information covering the standard Controllino firmware.

## Flashing
- [flashing an image to the SD card](/Software/Controllino-firmware/flashing image)

## Frequently asked questions
- [check the FAQ](/FAQ.md)

## Additional information and actions

- [creating a backup](/Software/Controllino-firmware/sd card backup)
- [enabling SSH access](/Software/Controllino-firmware/ssh-access)
- [checking docker.config](/Software/Controllino-firmware/docker config)
- [checking error logs](/Software/Controllino-firmware/check error logs)
