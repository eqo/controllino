# Frequently asked Question

### Q: What maker APP can I use to change location, antenna, hight of the hotspot?
A: Nebra, Bobcat, Senscap, Hotspotty and others.

### Q: Can I still onboard a new Controllino?
A: Currently no. Helium Foundation is discussing solutions for this currently.

### Q: How do I open the Controllino V1?
A: Check out the [Video by @Uros](https://www.youtube.com/watch?v=msOSEU4U0MU) for a step by step guide.

### Q: How can I get SSH access?
A: Follow the steps in [enabling SSH access](/Software/Controllino-firmware/ssh-access)

### Q: My SD card died, what can I do?
A1: You want Controllino firmware. Watch this [video by @Uros ](https://www.youtube.com/watch?v=msOSEU4U0MU&t=309s) and fill out the google form in the description.

A2: You want to be future proof and [switch to nebra Firmware](/Software/Nebra-firmware). 

### Q: How can I check error logs of the miner?
A: Use the [dashboard or SSH](/Software/Controllino-firmware/check error logs/).
